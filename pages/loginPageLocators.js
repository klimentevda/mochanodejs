export const loginPageLocators = {
	emailField: '//input[@name=\'email\']',
	passwordField: '//input[@name=\'password\']',
	logInButton: '//input[@type=\'submit\']',
	facebookAuthorisationButton: '//*[@class=\'external-auth facebook\']',
	googleAuthorisationButton: '//*[@class=\'external-auth google\']',
	microsoftAuthorisationSelectorButton: '//*[@class=\'external-auth microsoft-selector\']',
	microsoftAccountAuthorisationButton: '//*[@class=\'external-auth microsoft\']',
	microsoftWorkOrSchoolAccountAuthorisationButton: '//*[@class=\'external-auth aad\']',
	errorMessage: '//*[@class=\'errors\']//*[@class=\'message\']'
};