/**
 * @description Test scenarios for Wunderlist inbox page
 */

import { inboxPage } from '../../../pages/inboxPage';

import { loginPageFunctions } from '../../../functions/loginPageFunctions';

import { credentials } from './loginTest.wl.testData';
import { task, list } from './inboxTest.wl.testData';

const assert = require('assert');

describe('Wunderlist', function() {
	before(function () {
		loginPageFunctions.openLoginPage();
		loginPageFunctions.signIn(credentials.valid);
	});

	it('should display created task', function() {
		inboxPage.typeTaskName(task.description);
		assert.ok(inboxPage.isTaskDisplayed(task.description));
	});

	it('should not display task marked as completed', function() {
		inboxPage.markTaskAsCompleted(task.description);
		assert.ok(!inboxPage.isTaskDisplayed(task.description));
	});

	it('should display in completed list task marked as completed', function() {
		inboxPage.clickShowCompletedButton();
		assert.ok(inboxPage.isTaskDisplayed(task.description));
	});

	it('should not display deleted task', function() {
		inboxPage.contextClickOnTask(task.description);
		inboxPage.clickDeleteTask();
		inboxPage.clickConfirmDeleteButton();
		assert.ok(!inboxPage.isTaskDisplayed(task.description));
	});

	it('should display created list', function() {
		inboxPage.clickCreateList();
		inboxPage.typeListName(list.name);
		assert.ok(inboxPage.isListDisplayed(list.name));
	});

	it('should display moved task from inbox to the new list', function() {
		inboxPage.openPage();
		inboxPage.typeTaskName(task.description);
		inboxPage.contextClickOnTask(task.description);
		inboxPage.clickMoveTaskToList(list.name);
		inboxPage.openListPage(list.name);
		assert.ok(inboxPage.isTaskDisplayed(task.description));
	});
});