/**
 * Business functions for Login page
 */

import { loginPage } from '../../pages/loginPage';
import {LoginPageMail} from '../../pages/mail/loginPageMail';
import {loginPageMailLocators} from "../../pages/mail/loginPageMailLocators";
class loginPageMailFunctions {

	openMailPage() {
		loginPage.openPage('https://mail.ru/');
	}

	/**
	 * @param {Object} credentials
	 * @param {string} credentials.email
	 * @param {string} credentials.password
	 */
	signInMail(credentials) {
		LoginPageMail.typeEmail(credentials.email);
		LoginPageMail.clickLogIn();
	}
}

 export const LoginPageMailFunctions = new loginPageMailFunctions();