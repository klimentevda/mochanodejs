export const inboxPageLocators = {
	userToolBar: '//*[@id=\'user-toolbar\']',
	userMenu: '//*[@class=\'avatar medium\']',
	logOutLink: '//a[@class=\'logout\']',
	addNewTaskField: '//*[@id=\'tasks\']//input',
	showCompletedButton: '//*[@class=\'groupHeader completed-items-heading\']',
	deleteTaskButton: '//*[@title=\'Delete To-do\']',
	moveTaskButton: '//*[@title=\'Move to-do to...\']',
	blueConfirmButton: '//*[@class=\'blue full confirm\']',
	createListLink: '//*[@class=\'sidebarActions-addList\']',
	listNameField: '//input[@class=\'big listOptions-title\']',
	taskDescription: (description) => {
		return `//*[@class='taskItem-titleWrapper']//span[contains(text(), \'${description}\')]`;
	},
	taskCheckBox: (description) => {
		return `//*[@class='taskItem-titleWrapper']//span[contains(text(), \'${description}\')]/parent::div/preceding-sibling::a//*[@title='Mark as Completed']`;
	},
	listName: (name) => {
		return `//*[@class='lists-collection']//span[contains(text(), \'${name}\')]`
	},
	moveTaskToListLink: (name) => {
		return `//*[@title='Move to-do to...']//*//span[contains(text(), \'${name}\')]`;
	}
};