export const loginPageMailLocators = {
	emailField: '//input[@id=\'mailbox:login\']',
	buttonAddPassword: '//label[@id=\'mailbox:submit\']'
};