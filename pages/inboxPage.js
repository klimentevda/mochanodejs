/**
 * @description Inbox page class for Page Object Pattern implementation
 */

import Page from './page';

import { inboxPageLocators } from './inboxPageLocators';

class InboxPage extends Page {
	openPage() {
		super.openPage('https://www.wunderlist.com/#/lists/inbox');
	}

	isUserToolBarDisplayed() {
		return browser.$(inboxPageLocators.userToolBar).isDisplayed();
	}

	clickUserMenu() {
		this.waitAndClick(inboxPageLocators.userMenu);
	}

	clickLogOut() {
		this.waitAndClick(inboxPageLocators.logOutLink);
	}

	/**
	 * @param {string} description
	 */
	typeTaskName(description) {
		this.waitAndType(inboxPageLocators.addNewTaskField, description);
		this.pressEnter();
	}

	/**
	 * @param {string} description
	 */
	isTaskDisplayed(description) {
		return browser.$(inboxPageLocators.taskDescription(description)).isDisplayed();
	}

	/**
	 * @param {string} description
	 */
	markTaskAsCompleted(description) {
		this.waitAndClick(inboxPageLocators.taskCheckBox(description));
	}

	clickShowCompletedButton() {
		this.waitAndClick(inboxPageLocators.showCompletedButton);
	}

	contextClickOnTask(description) {
		this.waitAndContextClick(inboxPageLocators.taskDescription(description));
	}

	clickDeleteTask() {
		this.waitAndClick(inboxPageLocators.deleteTaskButton);
	}

	clickConfirmDeleteButton() {
		this.waitAndClick(inboxPageLocators.blueConfirmButton);
	}

	clickCreateList() {
		this.waitAndClick(inboxPageLocators.createListLink);
	}

	/**
	 * @param {string} name
	 */
	typeListName(name) {
		this.waitAndType(inboxPageLocators.listNameField, name);
		this.pressEnter();
	}

	/**
	 * @param {string} name
	 */
	isListDisplayed(name) {
		return browser.$(inboxPageLocators.listName(name)).isDisplayed();
	}

	/**
	 * @param {string} name
	 */
	openListPage(name) {
		this.waitAndClick(inboxPageLocators.listName(name));
	}

	/**
	 * @param {string} name
	 */
	clickMoveTaskToList(name) {
		this.waitAndContextClick(inboxPageLocators.moveTaskButton);
		this.waitAndClick(inboxPageLocators.moveTaskToListLink(name));
	}
}

export const inboxPage = new InboxPage();
