/**
 * @description Business functions for Inbox page
 */

import { inboxPage } from '../pages/inboxPage';

class InboxPageFunctions {
	logOut() {
		inboxPage.clickUserMenu();
		inboxPage.clickLogOut();
	}
}

export const inboxPageFunctions = new InboxPageFunctions();