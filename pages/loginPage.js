/**
 * @description Login page class for Page Object Pattern implementation
 */

import Page from './page';

import { loginPageLocators } from './loginPageLocators';

class LoginPage extends Page {
	/**
	 * @param {string} url
	 */
	openPage(url) {
		super.openPage(url);
	}

	/**
	 * @param {string} email
	 */
	typeEmail(email) {
		this.waitAndType(loginPageLocators.emailField, email);
	}

	/**
	 * @param {string} password
	 */
	typePassword(password) {
		this.waitAndType(loginPageLocators.passwordField, password);
	}

	clickLogIn() {
		this.waitAndClick(loginPageLocators.logInButton);
	}

	clickFacebookAuthorisationButton() {
		this.waitAndClick(loginPageLocators.facebookAuthorisationButton);
	}

	clickGoogleAuthorisationButton() {
		this.waitAndClick(loginPageLocators.googleAuthorisationButton);
	}

	clickMicrosoftSelectorAuthorisation() {
		this.waitAndClick(loginPageLocators.microsoftAuthorisationSelectorButton);
	}

	clickMicrosoftAccountAuthorisationButton() {
		this.waitAndClick(loginPageLocators.microsoftAccountAuthorisationButton);
	}

	clickMicrosoftWorkOrSchoolAccountAuthorisationButton() {
		this.waitAndClick(loginPageLocators.microsoftWorkOrSchoolAccountAuthorisationButton);
	}

	isErrorMessageDisplayed() {
		return browser.$(loginPageLocators.errorMessage).isDisplayed();
	}

	isMicrosoftAccountAuthorisationButtonDisplayed() {
		return browser.$(loginPageLocators.microsoftAccountAuthorisationButton).isDisplayed();
	}

	isMicrosoftWorkOrSchoolAccountAuthorisationButtonDisplayed() {
		return browser.$(loginPageLocators.microsoftWorkOrSchoolAccountAuthorisationButton).isDisplayed();
	}
}

export const loginPage = new LoginPage();
