/**
 * Business functions for Login page
 */

import {loginPage} from '../pages/loginPage';

class LoginPageFunctions {

    openLoginPage() {
        loginPage.openPage('https://www.wunderlist.com/login?redirect_url=/webapp');
    }

    /**
     * @param {Object} credentials
     * @param {string} credentials.email
     * @param {string} credentials.password
     */
    signIn(credentials) {
        loginPage.typeEmail(credentials.email);
        loginPage.typePassword(credentials.password);
        loginPage.clickLogIn();
    }
}

export const loginPageFunctions = new LoginPageFunctions();