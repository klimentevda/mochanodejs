# Simple Test Automation Framework for Wunderlist Application

**Based on**
- [NodeJS](https://nodejs.org/) + [WebdriverIO](https://webdriver.io/) as primary tools for UI automation
- [Mocha](http://mochajs.org/) as test framework

**Test automation approach**
- Layered architecture is used as functional test architecture pattern: Page objects, business functions (keywords/test-cases), framework utilities

**Getting started**
1. Install:
    - [Node.js](https://nodejs.org/)
    - [Git](https://git-scm.com/)
2. Clone this repository to any folder
3. Install node modules running `npm install` in CLI
4. Set up environmental variable `wdio` with following value `FULL_PATH_TO_YOUR_FOLDER/node_modules/.bin/wdio`
4. How to run tests:
    - run `npm test` or `wdio config/wdio.conf.js` in CLI
