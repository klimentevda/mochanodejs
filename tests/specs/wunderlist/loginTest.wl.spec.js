/**
 * @description Test scenarios for Wunderlist login page
 */

import { loginPage } from '../../../pages/loginPage';
import { inboxPage } from '../../../pages/inboxPage';

import { loginPageFunctions } from '../../../functions/loginPageFunctions';

import { credentials } from './loginTest.wl.testData';

const assert = require('assert');

describe('Wunderlist', function() {
	let currentUrl;

	it('should redirect to the Facebook if selected Facebook authorisation', function() {
		loginPageFunctions.openLoginPage();
		loginPage.clickFacebookAuthorisationButton();
		currentUrl = loginPage.getUrl();
		assert.ok(currentUrl.includes('facebook.com/login'));
	});

	it('should redirect to the Google if selected Google authorisation', function() {
		loginPageFunctions.openLoginPage();
		loginPage.clickGoogleAuthorisationButton();
		currentUrl = loginPage.getUrl();
		assert.ok(currentUrl.includes('accounts.google.com'));
	});

	it('should display Microsoft Account authorisation button if selected Microsoft authorisation', function() {
		loginPageFunctions.openLoginPage();
		loginPage.clickMicrosoftSelectorAuthorisation();
		assert.ok(loginPage.isMicrosoftAccountAuthorisationButtonDisplayed());
	});

	it('should display Work or School Account authorisation button if selected Microsoft authorisation', function() {
	 	loginPageFunctions.openLoginPage();
		loginPage.clickMicrosoftSelectorAuthorisation();
		assert.ok(loginPage.isMicrosoftWorkOrSchoolAccountAuthorisationButtonDisplayed());
	});

	it('should redirect to the Live login if selected Microsoft Account authorisation', function() {
		loginPageFunctions.openLoginPage();
		loginPage.clickMicrosoftSelectorAuthorisation();
		loginPage.clickMicrosoftAccountAuthorisationButton();
		currentUrl = loginPage.getUrl();
		assert.ok(currentUrl.includes('login.live.com'));
	});

	it('should redirect to the Microsoft login if selected Microsoft Work or School Account authorisation', function() {
		loginPageFunctions.openLoginPage();
		loginPage.clickMicrosoftSelectorAuthorisation();
		loginPage.clickMicrosoftWorkOrSchoolAccountAuthorisationButton();
		currentUrl = loginPage.getUrl();
		assert.ok(currentUrl.includes('login.microsoftonline.com'));
	});

	it('should not log in user with invalid credentials', function() {
		loginPageFunctions.openLoginPage();
		loginPageFunctions.signIn(credentials.invalid);
		assert.ok(loginPage.isErrorMessageDisplayed());
	});

	it('should log in user with valid credentials', function() {
		loginPageFunctions.openLoginPage();
		loginPageFunctions.signIn(credentials.valid);
		assert.ok(inboxPage.isUserToolBarDisplayed());
	});
});