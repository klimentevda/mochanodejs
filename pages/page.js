/**
 * @description Base Page class for Page Object Pattern implementation
 */
const log4js = require('log4js');
const logger = log4js.getLogger();

export default class Page {
	getTitle() {
		logger.debug("get Title " + browser.getTitle());
		return browser.getTitle();
	};

	getUrl() {
		logger.debug("get Url " + browser.getUrl() )
		return browser.getUrl();
	};

	openPage(url) {
		logger.debug("Go to url : " + url )
		browser.url(url);
	}

	/**
	 * @param {string | Object | function} selector
	 */
	waitAndClick(selector) {
		logger.debug("wai tAnd Click " + selector )
		browser.$(selector).waitForDisplayed();
		browser.$(selector).waitForEnabled();
		browser.$(selector).click();
	}

	/**
	 * @param {string | Object | function} selector
	 * @param {string | number | boolean | object | any[]} value
	 */
	waitAndType(selector, value) {
		logger.debug("wai tAnd type in : " + selector + " : " + value)
		browser.$(selector).waitForDisplayed();
		browser.$(selector).waitForEnabled();
		browser.$(selector).setValue(value);
	}

	pressEnter() {
		browser.keys('Enter');
	}

	/**
	 * @param {string | Object | function} selector
	 */
	waitAndContextClick(selector) {
		browser.$(selector).waitForDisplayed();
		browser.$(selector).waitForEnabled();
		browser.$(selector).click({ button: 'right' });
	}
}