/**
 * @description Login page class for Page Object Pattern implementation
 */

import Page from "../page";

import {loginPageMailLocators} from './loginPageMailLocators.js';

class loginPageMail extends Page {

    /**
     * @param {string} url
     */
    openPage(url) {
        super.openPage(url);
    }

    /**
     * @param {string} email
     */
    typeEmail(email) {
        this.waitAndType(loginPageMailLocators.emailField, email);
    }

    clickLogIn() {
        this.waitAndClick(loginPageMailLocators.buttonAddPassword);
    }

}

export const LoginPageMail = new loginPageMail();
