/**
 * @description Test scenarios for mail inbox page
 */


import {LoginPageMailFunctions} from '../../../functions/mail/loginPageMailFunctions';

import {credentials} from './loginTest.m.testData';

import {loginPage} from "../../../pages/loginPage";

const assert = require('assert');
const log4js = require('log4js');
const logger = log4js.getLogger();

describe('mail', function () {
    const searchString = 'https://mail.ru/';
    let currentUrl;
    logger.level = 'debug';

    before(function () {
        LoginPageMailFunctions.openMailPage();
        LoginPageMailFunctions.signInMail(credentials.valid);
    });

    it('go to mail', function () {
        currentUrl = loginPage.getUrl();
        assert.ok(currentUrl.includes(searchString));
    });
});