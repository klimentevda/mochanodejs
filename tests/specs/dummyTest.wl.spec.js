/**
 * @description Simple dummy test for check of correct settings for the project
 */

const assert = require('assert');

describe('EPAM page', () => {
	it('should have the appropriate title', () => {
		browser.url('https://www.epam.com/');
		const actualTitle = browser.getTitle();
		const expectedTitle = 'EPAM | Enterprise Software Development, Design & Consulting';
		assert.strictEqual(actualTitle, expectedTitle);
	});
});